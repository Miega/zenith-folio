import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Folio } from '../models/folio';
import { Portfolio } from '../models/portfolio';

@Injectable()
export class PortfolioService {

  constructor() { }

  getFolio(): Observable<Folio[]> {
    return of(Portfolio);
  }
}

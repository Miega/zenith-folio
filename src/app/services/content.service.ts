import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Info } from '../models/info';
import { About } from '../models/about-me';
import { Contact } from '../models/contact';

@Injectable()
export class ContentService {

  constructor() { }

  getContent(): Observable<Info[]> {
    return of(About);
  }

  getContact(): Observable<Info[]> {
    return of(Contact);
  }

}

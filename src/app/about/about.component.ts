import { Component, OnInit } from '@angular/core';

import { Info } from '../models/info';
import { About } from '../models/about-me';
import { ContentService } from '../services/content.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  providers: [ContentService]
})
export class AboutComponent implements OnInit {

  contents: Info[];

  constructor(private contentService: ContentService) { }

  getContent(): void {
    this.contentService.getContent()
      .subscribe(content => this.contents = content);
  }

  ngOnInit() {
    this.getContent();
  }

}

import { Component, OnInit } from '@angular/core';

import { Info } from '../models/info';
import { Contact } from '../models/contact';
import { ContentService } from '../services/content.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  providers: [ContentService]
})
export class ContactComponent implements OnInit {

  contacts: Info[];

  constructor(private contactService: ContentService) { }

  getContact(): void {
    this.contactService.getContact()
      .subscribe(contact => this.contacts = contact);
  }

  ngOnInit() {
    this.getContact();
  }

}

import { Info } from './info';

export const Contact: Info[] = [
    { header: 'Commission Info', content: 'If you need a website built for your small business or for personal use, please refer to my pricing chart below. Depending on the nature of your request, prices can be negotiated.', link: '/pricing', linkLabel: 'Pricing Chart'},
    { header: 'Contact Form', content: 'You can contact me using the form below if you have a general inquiry, are requesting commission work, or just want to say hello. I will make sure to get back to you as soon as I am able.', link: 'https://docs.google.com/forms/d/1rupvtLh8T8V7xkd55n0hxTIzmVjoZY_RNkdGaJpr3h8/edit?usp=drivesdk', linkLabel: 'Link to Form'}
];

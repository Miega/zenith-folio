export class Info {
    header: string;
    content: string;
    link: string;
    linkLabel: string;
}

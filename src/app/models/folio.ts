export class Folio {
    title: string;
    content: string;
    thumb: string;
    thumbLink: string;
    isComplete: Boolean;
    isWip: Boolean;
}

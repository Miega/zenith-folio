import { Info } from './info';

export const About: Info[] = [
    { header: 'Software Development', content: 'I have been working in web and software development for four years, specializing in front-end development. I have been slowly learning more about C# and .NET in my spare time, and hope to transition to full-stack development soon! I also hand-crafted this portfolio site with the help of Angular CLI.', link: 'n/a', linkLabel: 'n/a' },
    { header: 'Game Development', content: 'I am the co-director of Somnova Studios for our flagship project, Missing Stars, and have been working on it for six years. I primarily write, but I also handle daily operations, create meeting agendas, and check in with team leads.', link: 'n/a', linkLabel: 'n/a' },
    { header: 'Hobbies', content: 'Outside of professional work, I play Magic: The Gathering on a regular basis, having played since Fifth Dawn. I also enjoy various console, PC, and mobile games. Cooking is another passion of mine, and I love making dishes for friends and family.', link: 'n/a', linkLabel: 'n/a' }
];

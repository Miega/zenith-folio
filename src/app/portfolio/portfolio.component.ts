import { Component, OnInit } from '@angular/core';

import { Folio } from '../models/folio';
import { Portfolio } from '../models/portfolio';
import { PortfolioService } from '../services/portfolio.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css'],
  providers: [PortfolioService]
})
export class PortfolioComponent implements OnInit {

  portfolios: Folio[];

  constructor(private portfolioService: PortfolioService) { }

  getFolio(): void {
    this.portfolioService.getFolio()
      .subscribe(portfolios => this.portfolios = portfolios);
  }

  ngOnInit() {
    this.getFolio();
  }
}
